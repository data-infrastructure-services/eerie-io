#!/bin/bash -l
#SBATCH --job-name=remapcon-interpol
#SBATCH --partition=compute
#SBATCH --time=8:00:00
#SBATCH --mem=256GB
#SBATCH --output=%x_%j.log
#SBATCH --account=bm1344
#SBATCH --begin=now+8hours

sbatch icon-interpolation-remapcon025.sh
export HDF5_PLUGIN_PATH=/work/ik1017/hdf5plugin/plugins

module load cdo
module load netcdf-c

initskip="1950"
exp="erc1011"
model="ICON"
datadir="/work/bm1344/k203123/experiments/${exp}/"
startyear=2002
lastyear=$(ls $datadir | cut -d '_' -f 2 | cut -c 1-4 | sort -u | grep -o '[[:digit:]]*' | tail -n 1)
#endyear=2021
endyear=$((lastyear-1))
opatterns="oce_2d_1d_mean oce_2d_1d_square oce_2d_1mth_mean oce_2d_1mth_square oce_eddy_1mth_mean postprocessing"
#opatterns3d="oce_ml_1mth_mean oce_5lev_1d_mean"
opatterns3d="oce_5lev_1d_mean"
apatterns="atm_ml_1mth_mean atm_2d_1mth_mean atm_2d_1d_mean atm_2d_1d_min atm_2d_1d_max"
lpatterns="lnd_2d_1mth_mean lnd_2d_1d_mean"
outdirtrunk="/work/bm1344/DKRZ/${model}/${exp}/postprocessing/interpolation/"
mkdir -p $outdirtrunk
maxjobs=10

targetgrid=/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc

srcgrid=/pool/data/ICON/grids/public/mpim/0016/icon_grid_0016_R02B09_O.nc
srcgrid_atm=/pool/data/ICON/grids/public/mpim/0033/icon_grid_0033_R02B08_G.nc
remapwgt=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm.nc
remapwgt_3d=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm_3d.nc
remapwgt_atm=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b8G_IFS25invertlat_yconremapweights.nc
remapwgt_atm_lnd=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b8G_IFS25invertlat_yconremapweights_lnd_lsm.nc
remapwgt_edge=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm_edge.nc
lsm=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_lsm.nc
lsm_3d=/pool/data/ICON/oes/input/r0005/OceanOnly_IcosSymmetric_4932m_rotatedZ37d_modified_srtm30_1min/L72/exp.ocean_era51\
h_zstar_r2b9_23075-ERA_fx_20100101T000200Z.nc
lsm_edge=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_lsm_edge.nc
lsm_lnd=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b8G_lsm.nc

#cdo -lec,0 -selname,cell_sea_land_mask $srcgrid $lsm
#cdo -gec,0 -selname,cell_sea_land_mask $srcgrid_atm $lsm_lnd
#cdo -lec,0 -selname,edge_sea_land_mask $srcgrid $lsm_edge
#cdo -L -P 128 -genycon,/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc -div [ -setgrid,$srcgrid -selname,ssh /work/bm1344/k203123/experiments/erc1011/run_20030101T000000-20030131T235845/erc1011_oce_2d_1mth_mean_20030201T000000Z.nc $lsm ] ${remapwgt}
#cdo -L -P 128 -genycon,/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc -setgrid,$srcgrid_atm -selname,tas /work/bm1344/k203123/experiments/erc1011/run_20030101T000000-20030131T235845/erc1011_atm_2d_1mth_mean_20030201T000000Z.nc ${remapwgt_atm}

# cdo -L -P 128 -setlevel,$lev -genycon,/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc -div [ -setgrid,$srcgrid -seltimestep,1 -selname,to /work/bm1344/k203123/experiments/erc1011/run_20030101T000000-20030131T235845/erc1011_oce_ml_1mth_mean_20030201T000000Z.nc -selname,wet_c $lsm_3d ] ${remapwgt_3d}
#3d to 3d full with xarray
#cdo -L -P 128 -genycon,/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc -div [ -setgrid,$srcgrid_atm -selname,hydro_w_snow_can_box /work/bm1344/k203123/experiments/erc1011/run_20030101T000000-20030131T235845/erc1011_lnd_2d_1mth_mean_20030201T000000Z.nc $lsm_lnd ] ${remapwgt_atm_lnd}
#cdo -L -P 128 -genycon,/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc -div [ -setgrid,$srcgrid:2 -selname,verticallyTotal_mass_flux_e /work/bm1344/k203123/experiments/erc1011/run_20030101T000000-20030131T235845/erc1011_oce_2d_1mth_mean_20030201T000000Z.nc $lsm_edge ] ${remapwgt_edge}

for year in $(seq $startyear $endyear); do
for pattern in $opatterns; do
    outdir="${outdirtrunk}/${pattern}_remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
      echo $dir;
      dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev);
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort);
      files=""
      mkdir -p $outdir/${dirn}
      for file in $files_unfiltered; do
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append");
      done
      files=$(echo $files | sort -u);
      for file in $files; do
        (
echo $file
          allvarsButVertical=$(cdo showname ${file} | grep -v verticallyTotal_mass_flux_e | tr -d '\n' | tr ' ' ',' | cut -d ',' -f 2-);
          t=$(echo $file | rev | cut -d '_' -f 1 | rev)
#          echo "          cdo -L -P 16 -remap,${targetgrid},${remapwgt} -setgrid,${srcgrid} #-selname,${allvarsButVertical} $file $outdir/${dirn}/${exp}_${outpattern}_${t}_temp"
          cdo -L -P 16 -remap,${targetgrid},${remapwgt} -div [ -setgrid,${srcgrid} -selname,${allvarsButVertical} $file $lsm ] $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
          echo "Start compression"
          comprvars=$(echo ${allvarsButVertical} | tr ',' '&')
          comprvars="$comprvars,32001,0,0,4,81920,5,1,1"
          nccopy -F $comprvars $outdir/${dirn}/${exp}_${outpattern}_${t}_temp  $outdir/${dirn}/${exp}_${outpattern}_${t}
          rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp 
          
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
    done
done
wait
maxjobs=4
for pattern in $opatterns3d; do
    outdir="${outdirtrunk}/${pattern}_remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
      echo $dir;
      dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev);
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort);
      files=""
      mkdir -p $outdir/${dirn}
      for file in $files_unfiltered; do
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append");
      done
      files=$(echo $files | sort -u);
      for file in $files; do
        (
echo $file
          allvarsButVertical="to,so,u,v,rsdoabsorb"
          t=$(echo $file | rev | cut -d '_' -f 1 | rev)
#          echo "          cdo -L -P 16 -remap,${targetgrid},${remapwgt} -setgrid,${srcgrid} #-selname,${allvarsButVertical} $file $outdir/${dirn}/${exp}_${outpattern}_${t}_temp"
          levels=$(cdo showlevel -selname,to $file | tr ' ' ',')
          
          cdo -L -P 16 -remap,${targetgrid},${remapwgt_3d} -div [ -setgrid,${srcgrid} -selname,${allvarsButVertical} $file -sellevel$levels -selname,wet_c $lsm_3d ] $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
          echo "Start compression"
          comprvars=$(echo ${allvarsButVertical} | tr ',' '&')
          comprvars="$comprvars,32001,0,0,4,81920,5,1,1"
          nccopy -F $comprvars $outdir/${dirn}/${exp}_${outpattern}_${t}_temp  $outdir/${dirn}/${exp}_${outpattern}_${t}
          rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp 
          
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
    done
done

if false; then
for pattern in "oce_ml_1d_mean" ; do
    outdir="${outdirtrunk}/${pattern}_remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
      echo $dir;
      dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev);
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort);
      files=""
      mkdir -p $outdir/${dirn}
      for file in $files_unfiltered; do
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append");
      done
      files=$(echo $files | sort -u);
      for file in $files; do
        (
echo $file
          allvarsButVertical="to,so"
          t=$(echo $file | rev | cut -d '_' -f 1 | rev)
#          echo "          cdo -L -P 16 -remap,${targetgrid},${remapwgt} -setgrid,${srcgrid} #-selname,${allvarsButVertical} $file $outdir/${dirn}/${exp}_${outpattern}_${t}_temp"
          cdo -L -P 16 -remap,${targetgrid},${remapwgt_3d} -seltimestep,1 -div [ -setgrid,${srcgrid} -selname,${allvarsButVertical} $file -selname,wet_c $lsm_3d ] $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
          echo "Start compression"
          comprvars=$(echo ${allvarsButVertical} | tr ',' '&')
          comprvars="$comprvars,32001,0,0,4,81920,5,1,1"
          nccopy -F $comprvars $outdir/${dirn}/${exp}_${outpattern}_${t}_temp  $outdir/${dirn}/${exp}_${outpattern}_${t}
          rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp 
          
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
    done
done
fi
wait
maxjobs=10

opatterns="oce_2d_1d_mean oce_2d_1mth_mean"
for pattern in $opatterns; do
    outdir="${outdirtrunk}/${pattern}_vertical-remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
      echo $dir;
      dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev);
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort);
      files=""
      mkdir -p $outdir/${dirn}
      for file in $files_unfiltered; do
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append");
      done
      files=$(echo $files | sort -u);
      for file in $files; do
        (
echo $file
          t=$(echo $file | rev | cut -d '_' -f 1 | rev)
          cdo -L -P 16 -remap,${targetgrid},${remapwgt_edge} -div [ -setgrid,${srcgrid}:2 -selname,verticallyTotal_mass_flux_e $file $lsm_edge ] $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
          echo "Start compression"
          comprvars="verticallyTotal_mass_flux_e,32001,0,0,4,81920,5,1,1"
          nccopy -F $comprvars $outdir/${dirn}/${exp}_${outpattern}_${t}_temp  $outdir/${dirn}/${exp}_${outpattern}_${t}
          rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp 
          
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
    done
done

for pattern in $apatterns; do
    outdir="${outdirtrunk}/${pattern}_remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
      echo $dir;
      dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev);
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort);
      files=""
      mkdir -p $outdir/${dirn}
      for file in $files_unfiltered; do
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append");
      done
      files=$(echo $files | sort -u);
      for file in $files; do
        (
echo $file
          allvarsButVertical=$(cdo showname ${file} | grep -v verticallyTotal_mass_flux_e | tr -d '\n' | tr ' ' ',' | cut -d ',' -f 2-);
          t=$(echo $file | rev | cut -d '_' -f 1 | rev)
#          echo "          cdo -L -P 16 -remap,${targetgrid},${remapwgt_atm} -setgrid,${srcgrid_atm} #-selname,${allvarsButVertical} $file $outdir/${dirn}/${exp}_${outpattern}_${t}_temp"
          cdo -L -P 16 -remap,${targetgrid},${remapwgt_atm} -setgrid,${srcgrid_atm} -selname,${allvarsButVertical} $file $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
          echo "Start compression"
          comprvars=$(echo ${allvarsButVertical} | tr ',' '&')
          comprvars="$comprvars,32001,0,0,4,81920,5,1,1"
          nccopy -F $comprvars $outdir/${dirn}/${exp}_${outpattern}_${t}_temp  $outdir/${dirn}/${exp}_${outpattern}_${t}
          rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp 
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
    done
done
datadir=$outdirtrunk
apatterns="atm_plev19_1d_mean atm_plev19_1mth_mean"
for pattern in $apatterns; do
    outdir="${outdirtrunk}/${pattern}_remap025"
    mkdir -p $outdir
    outpattern="${pattern}_remap025"
    n=0
    dir="${outdirtrunk}/$pattern"
      echo $dir;
      dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev);
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort);
      files=""
      #the last file could be unfinished:
      #length=${#files_unfiltered[@]}
      for file in $files_unfiltered; do
      #  if [ "$file" == "${files_unfiltered[length-1]}" ]; then
      #    break
      #  fi;
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append");
      done
      files=$(echo $files | sort -u);
      #files="$files ]"
      for file in $files; do
        (
echo $file
          allvarsButVertical=$(cdo showname ${file} | grep -v verticallyTotal_mass_flux_e | tr -d '\n' | tr ' ' ',' | cut -d ',' -f 2-);
          t=$(echo $file | rev | cut -d '_' -f 1 | rev)
#          echo "          cdo -L -P 16 -remap,${targetgrid},${remapwgt_atm} -setgrid,${srcgrid_atm} #-selname,${allvarsButVertical} $file $outdir/${exp}_${outpattern}_${t}_temp"
          cdo -L -P 16 -remap,${targetgrid},${remapwgt_atm} -setgrid,${srcgrid_atm} -selname,${allvarsButVertical} $file $outdir/${exp}_${outpattern}_${t}_temp
          echo "Start compression"
          comprvars=$(echo ${allvarsButVertical} | tr ',' '&')
          comprvars="$comprvars,32001,0,0,4,81920,5,1,1"
          nccopy -F $comprvars $outdir/${exp}_${outpattern}_${t}_temp  $outdir/${exp}_${outpattern}_${t}
          rm $outdir/${exp}_${outpattern}_${t}_temp 
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
done

datadir="/work/bm1344/k203123/experiments/${exp}/"
for pattern in $lpatterns; do
    outdir="${outdirtrunk}/${pattern}_remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
      echo $dir;
      dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev);
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort);
      files=""
      mkdir -p $outdir/${dirn}
      for file in $files_unfiltered; do
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append");
      done
      files=$(echo $files | sort -u);
      for file in $files; do
        (
echo $file
          allvarsButVertical=$(cdo showname ${file} | grep -v verticallyTotal_mass_flux_e | tr -d '\n' | tr ' ' ',' | cut -d ',' -f 2-);
          t=$(echo $file | rev | cut -d '_' -f 1 | rev)
#          echo "          cdo -L -P 16 -remap,${targetgrid},${remapwgt} -setgrid,${srcgrid} #-selname,${allvarsButVertical} $file $outdir/${dirn}/${exp}_${outpattern}_${t}_temp"
          cdo -L -P 16 -remap,${targetgrid},${remapwgt_atm_lnd} -div [ -setgrid,${srcgrid_atm} -selname,${allvarsButVertical} $file $lsm_lnd ] $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
          echo "Start compression"
          comprvars=$(echo ${allvarsButVertical} | tr ',' '&')
          comprvars="$comprvars,32001,0,0,4,81920,5,1,1"
          nccopy -F $comprvars $outdir/${dirn}/${exp}_${outpattern}_${t}_temp  $outdir/${dirn}/${exp}_${outpattern}_${t}
          rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp 
          
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
    done
done
done
wait
