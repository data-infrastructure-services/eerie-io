#!/bin/bash

#SBATCH --job-name=ICON_ocen_2d_regrid     
#SBATCH --partition=compute
#SBATCH --time=8:00:00
#SBATCH --mem=256GB
#SBATCH --mail-type=FAIL       
#SBATCH --account=bm1344       
#SBATCH --output=ICON.o%j    
#SBATCH --error=ICON.e%j 

# Load necessary modules
export HDF5_PLUGIN_PATH=/work/ik1017/hdf5plugin/plugins
module load cdo
module load netcdf-c

initskip="1950"
exp="erc2002"
model="ICON"
datadir="/work/bm1344/k203123/experiments/${exp}/"
startyear=1991
endyear=2040
opatterns="oce_2d_1d_mean"
outdirtrunk="/work/bm1344/k202193/${model}/${exp}/postprocessing/interpolation/sst/"
mkdir -p $outdirtrunk
maxjobs=16

targetgrid=/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc
srcgrid=/pool/data/ICON/grids/public/mpim/0016/icon_grid_0016_R02B09_O.nc
remapwgt=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm.nc
lsm=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_lsm.nc

process_file() {
    file=$1
    outdir=$2
    outpattern=$3
    dirn=$4
    srcgrid=$5
    targetgrid=$6
    remapwgt=$7
    lsm=$8
    exp=$9

    t=$(echo $file | rev | cut -d '_' -f 1 | rev)
    # Fix the output filename to start with ${exp}_${outpattern}
    output_file=$outdir/${dirn}/${exp}_${outpattern}_${t}

    # Check if the output file already exists
    if [[ ! -f $output_file ]]; then
        echo "Processing $file"
        cdo -L -P 16 -remap,${targetgrid},${remapwgt} -div [ -setgrid,${srcgrid} -selname,to $file $lsm ] ${output_file}_temp
        cdo chname,to,sst ${output_file}_temp ${output_file}_renamed
        echo "Start compression"
        nccopy -F chunked ${output_file}_renamed $output_file
        rm ${output_file}_temp ${output_file}_renamed
    fi
}


export -f process_file
export targetgrid srcgrid remapwgt lsm

for year in $(seq $startyear $endyear); do
    for pattern in $opatterns; do
        outdir="${outdirtrunk}/${pattern}-remap025"
        outpattern="${pattern}_remap025"
        for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
            dirn=$(basename $dir)
            mkdir -p $outdir/${dirn}
            find $dir -type f -name "${exp}_${pattern}*.nc" | sort | \
            xargs -P $maxjobs -I {} bash -c 'process_file "$@"' _ {} $outdir $outpattern $dirn $srcgrid $targetgrid $remapwgt $lsm $exp
        done
    done
done


