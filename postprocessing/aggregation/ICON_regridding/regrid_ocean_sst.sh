#!/bin/bash

#SBATCH --job-name=ICON_ocen_2d_regrid     
#SBATCH --partition=compute
#SBATCH --time=8:00:00
#SBATCH --mem=256GB
#SBATCH --mail-type=FAIL       
#SBATCH --account=bm1344       
#SBATCH --output=ICON.o%j    
#SBATCH --error=ICON.e%j 

# Load necessary modules
export HDF5_PLUGIN_PATH=/work/ik1017/hdf5plugin/plugins
module load cdo
module load netcdf-c

initskip="1950"
exp="erc2002"
model="ICON"
datadir="/work/bm1344/k203123/experiments/${exp}/"
startyear=1991
endyear=2040
opatterns="oce_2d_1d_mean"
outdirtrunk="/work/bm1344/k202193/${model}/${exp}/postprocessing/interpolation/sst/"
mkdir -p $outdirtrunk
maxjobs=16

targetgrid=/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc
srcgrid=/pool/data/ICON/grids/public/mpim/0016/icon_grid_0016_R02B09_O.nc
remapwgt=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm.nc
lsm=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_lsm.nc


for year in $(seq $startyear $endyear); do
for pattern in $opatterns; do
    outdir="${outdirtrunk}/${pattern}-remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
        echo $dir
        dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev)
        files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort)
        files=""
        mkdir -p $outdir/${dirn}
        for file in $files_unfiltered; do
            t=$(echo $file | rev | cut -d '_' -f 1 | rev)
            append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file)
            files=$(echo "$files $append")
        done
        files=$(echo $files | sort -u)
        for file in $files; do
            (
                echo $file
                t=$(echo $file | rev | cut -d '_' -f 1 | rev)
                cdo -L -P 16 -remap,${targetgrid},${remapwgt} -div [ -setgrid,${srcgrid} -selname,to $file $lsm ] $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
                echo "Start compression"
                nccopy -F chunked $outdir/${dirn}/${exp}_${outpattern}_${t}_temp $outdir/${dirn}/${exp}_${outpattern}_${t}
                rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
            ) &
            if (( $(($((++n)) % $maxjobs)) == 0 )); then
                wait
            fi
        done
    done
done
done
wait
