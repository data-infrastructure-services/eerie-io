#!/bin/bash

module load nco

BASE_DIR="/work/bm1344/k202193/ICON/erc2002/postprocessing/interpolation/sst/oce_2d_1d_mean-remap025"

NEW_STANDARD_NAME="sea_surface_temperature"
NEW_LONG_NAME="sea surface temperature"

CURRENT_DATE=$(date -u +"%Y-%m-%dT%H:%M:%SZ")

NEW_HISTORY="${CURRENT_DATE} ; deleted for convinience"

find "$BASE_DIR" -type f -name "*.nc" | while read -r file; do
    echo "Processing file: $file"
    
    ncks -O -h -x -v history "$file" tmp.nc
    ncatted -O -h \
        -a standard_name,sst,o,c,"$NEW_STANDARD_NAME" \
        -a long_name,sst,o,c,"$NEW_LONG_NAME" \
        -a history,global,o,c,"$NEW_HISTORY" \
        tmp.nc "$file"
    
    rm -f tmp.nc

    if [ $? -eq 0 ]; then
        echo "Updated: $file"
    else
        echo "Failed to update: $file"
    fi
done

echo "All files processed."
