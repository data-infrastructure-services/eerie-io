#!/bin/bash

#SBATCH --job-name=ICON_ocen_2d_regrid     
#SBATCH --partition=compute
#SBATCH --time=8:00:00
#SBATCH --mem=256GB
#SBATCH --mail-type=FAIL       
#SBATCH --account=bm1344       
#SBATCH --output=ICON.o%j    
#SBATCH --error=ICON.e%j 

# Load necessary modules
export HDF5_PLUGIN_PATH=/work/ik1017/hdf5plugin/plugins
module load cdo
module load netcdf-c

initskip="1950"
exp="erc2002"
model="ICON"
datadir="/work/bm1344/k203123/experiments/${exp}/"
startyear=1980
endyear=1980
opatterns="oce_2d_1d_mean"
#apatterns="atm_ml_1mth_mean atm_2d_1mth_mean atm_2d_1d_mean atm_2d_1d_min atm_2d_1d_max"
outdirtrunk="/work/bm1344/k202193/${model}/${exp}/postprocessing/interpolation/new/1980/"
mkdir -p $outdirtrunk
maxjobs=10

targetgrid=/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc

srcgrid=/pool/data/ICON/grids/public/mpim/0016/icon_grid_0016_R02B09_O.nc
srcgrid_atm=/pool/data/ICON/grids/public/mpim/0033/icon_grid_0033_R02B08_G.nc
remapwgt=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm.nc
remapwgt_atm=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b8G_IFS25invertlat_yconremapweights.nc
remapwgt_atm_lnd=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b8G_IFS25invertlat_yconremapweights_lnd_lsm.nc
remapwgt_edge=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm_edge.nc
lsm=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_lsm.nc
lsm_edge=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_lsm_edge.nc
lsm_lnd=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b8G_lsm.nc

for year in $(seq $startyear $endyear); do
for pattern in $opatterns; do
    outdir="${outdirtrunk}/${pattern}-remap025"
    outpattern="${pattern}_remap025"
    n=0
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" -not -name "*${initskip}*" | sort); do
        echo $dir
        dirn=$(echo $dir | rev | cut -d '/' -f 1 | rev)
        files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc" | sort)
        files=""
        mkdir -p $outdir/${dirn}
        for file in $files_unfiltered; do
            t=$(echo $file | rev | cut -d '_' -f 1 | rev)
            append=$(ls $outdir/${dirn}/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file)
            files=$(echo "$files $append")
        done
        files=$(echo $files | sort -u)
        for file in $files; do
            (
                echo $file
                t=$(echo $file | rev | cut -d '_' -f 1 | rev)
                cdo -L -P 16 -remap,${targetgrid},${remapwgt_edge} -div [ -setgrid,${srcgrid}:2 -selname,verticallyTotal_mass_flux_e $file $lsm_edge ] $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
                echo "Start compression"
                comprvars="verticallyTotal_mass_flux_e,32001,0,0,4,81920,5,1,1"
                nccopy -F $comprvars $outdir/${dirn}/${exp}_${outpattern}_${t}_temp  $outdir/${dirn}/${exp}_${outpattern}_${t}
                rm $outdir/${dirn}/${exp}_${outpattern}_${t}_temp
            ) &
            if (( $(($((++n)) % $maxjobs)) == 0 )); then
                wait
            fi
        done
    done
done
done
wait
