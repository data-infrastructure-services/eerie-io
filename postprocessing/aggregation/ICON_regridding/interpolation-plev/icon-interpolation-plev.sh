#!/bin/bash
#SBATCH --job-name=remap-interpol
#SBATCH --partition=compute
#SBATCH --time=08:00:00
#SBATCH --mem=0
#SBATCH --output=%x_%j.log
#SBATCH --account=bm1344
#SBATCH --begin=now+8hours

sbatch icon-interpolation-plev.sh
#module use ~m221078/etc/Modules
#module add netcdf-dev cdo-dev
export HDF5_PLUGIN_PATH=/work/ik1017/hdf5plugin/plugins

module load cdo
#module load cdo/2.0.4-gcc-11.2.0
module load netcdf-c

exp="erc1011"
model="ICON"
datadir="/work/bm1344/k203123/experiments/${exp}/"
patterns="atm_ml_1mth_mean atm_ml_1d_mean_1"
outdirtrunk="/work/bm1344/DKRZ/${model}/${exp}/postprocessing/interpolation"
mkdir -p $outdirtrunk
maxjobs=6
PLEV19="100000,92500,85000,70000,60000,50000,40000,30000,25000,20000,15000,10000,7000,5000,3000,2000,1000,500,100"
startyear=2008
lastyear=$(ls $datadir | cut -d '_' -f 2 | cut -c 1-4 | sort -u | grep -o '[[:digit:]]*' | tail -n 1)
#endyear=2021
endyear=$((lastyear-1))

n=0
for pattern in $patterns; do
    outpattern=$(sed "s\ml\plev19\g" <<<${pattern})
    outpatterndir=$(sed "s;mean\_1;mean;g" <<<${outpattern})
    outdir="${outdirtrunk}/${outpatterndir}"
    mkdir -p $outdir

    for year in $(seq $startyear $endyear); do
    for dir in $(find $datadir -maxdepth 1 -type d -name "run_${year}*" | sort); do
      echo $dir
      files_unfiltered=$(find $dir -type f -name "${exp}_${pattern}*.nc"| sort);
      files=""
      length=${#files_unfiltered[@]}
      for file in $files_unfiltered; do
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        append=$(ls $outdir/${exp}_${outpattern}_${t} 1>/dev/null 2>&1 || echo $file);
        files=$(echo "$files $append")
        #files=$(echo "$files $file");
      done
      for file in $files; do
        (
        t=$(echo $file | rev | cut -d '_' -f 1 | rev)
        pattern_ap=$(sed "s,mean_1,mean_2,g" <<<${pattern}) 
        apfile=$(sed "s,${pattern},${pattern_ap},g" <<<${file}) 
        echo "$file $apfile"
        ls $apfile
        mergeornotmerge="-merge [ $file $apfile ]"
        if [[ "$pattern_ap" == "$pattern" ]]; then
          mergeornotmerge="$file"
        fi
        echo "cdo -L -P 16 -ap2pl,${PLEV19} $mergeornotmerge $outdir/${exp}_${outpattern}_${t}_temp "
        cdo -L -P 16 -ap2pl,${PLEV19} $mergeornotmerge $outdir/${exp}_${outpattern}_${t}_temp 
        echo "Start compression"
        allvars=$(cdo showname $outdir/${exp}_${outpattern}_${t}_temp | tr -d '\n' | tr ' ' ',' | cut -d ',' -f 2- | tr ',' '&')
        nccopy -F "${allvars},32001,0,0,4,81920,5,1,1" $outdir/${exp}_${outpattern}_${t}_temp  $outdir/${exp}_${outpattern}_${t}
        rm $outdir/${exp}_${outpattern}_${t}_temp 
        )&
        if (( $(($((++n)) % $maxjobs)) == 0 )) ; then
          wait
        fi
      done
    done
    done
done
