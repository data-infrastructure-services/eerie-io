import intake
import subprocess
import fsspec
from copy import deepcopy
from lossybits import *
import shutil

parameter_set=dict(
#    CATALOG="/work/bm1344/DKRZ/intake_catalogues/dkrz/disk/model-output/main.yaml",
    L_EERIE=False,
    CATALOG="/home/k/k204210/eerie-io/postprocessing/aggregation/ifs-ngc2.yaml",
    DISK_LOCK_TRUNK="/work/bm1344/k204210/archive/",
#    DISK_CACHE_TRUNK="/work/bm1344/DKRZ/",
    DISK_CACHE_TRUNK="/scratch/k/k204210",
    ARCH_TRUNK="/arch/bm1344",
    LOSSYLIST=["_1h_","_3h_","_6h_","5lev", "atmos_native_model-level_daily"],
    WHITELIST_VAR=None,#["so"], #None
#    LOSSYLIST=["atmos","ocean"],
#    EXP_NAME="eerie-control-1950",
#    EXP_NAME="eerie-spinup-1950",
    EXP_NAME="ngc2-nodeep",
#    VERSION="v20231106",
#    VERSION="v20240304",
    VERSION="v20241119",
    MODEL="ifs-tco1279-nemo-orca025",
    rawmodel="ifs-tco1279-nemo-orca025",
#    VERSION="v20240618",
#    MODEL="icon-esm-er",
#    MODEL="ifs-fesom2-sr",
#    rawmodel="ICON",
#    rawmodel="IFS-FESOM2",
    #MODEL="ifs-fesom2-sr"
    #
    #
#    whitelist="land",
    whitelist=None,
#    startsim=2002,
    startsim=2020,
#    endsim=2040,
    endsim=2025,
    ldrop_last_year=False,
    ldrop_last_year_for_calc=False,
    dset_idx=0, #-3 for FESOM,
    factor_compression = 2, #assume that memory is X time as large as file on disk,
    archive_accounts = 1, #gb per file
    target_chunk_size = 60, #mb
    LOSSYBITS=LOSSYBITS
)

dsetname=""
zipinterval=[]

def generate_arguments(dictionary,fn):
    """Create a 'arguments.py' module to initialize a Jupyter notebook."""
    with fsspec.open(fn, 'w') as fid:
        for key in dictionary:
            fid.write(f'{key} = {repr(dictionary[key])}\n')


def find_data_sources(catalog,name=None):
    newname='.'.join(
        [ a 
         for a in [name, catalog.name]
         if a
        ]
    )
    data_sources = []

    for key, entry in catalog.items():
        if isinstance(entry, intake.catalog.Catalog):
            if newname == "main":
                newname = None
            # If the entry is a subcatalog, recursively search it
            data_sources.extend(find_data_sources(entry, newname))
        elif isinstance(entry, intake.source.base.DataSource):
            data_sources.append(newname+"."+key)

    return data_sources

#cat=intake.open_catalog(parameter_set["CATALOG"])
#subcat=cat[parameter_set["MODEL"]][parameter_set["EXP_NAME"]]
subcat=intake.open_catalog(parameter_set["CATALOG"])
sources=find_data_sources(subcat)

filtered_sources=[
    '.'.join(a.split('.')[1:])
    for a in sources 
    if "tco1279-orca025-nemo-atm3d" in a
]
for dsname in filtered_sources:
    parameter_set["dsetname"]=dsname
    for a in range(parameter_set["startsim"],parameter_set["endsim"],5):
        parameter_set["zipinterval"]=[(a,a+5)]
        tempdir=f'{parameter_set["MODEL"]}_{parameter_set["EXP_NAME"]}_{parameter_set["VERSION"]}_{dsname}_{a}'
        generate_arguments(parameter_set,f"{tempdir}/arguments.py")
        shutil.copyfile("resize-raw.ipynb", f"{tempdir}/resize-raw.ipynb")
        subprocess.call([
            'sbatch',
            '-J',
            dsname,
            f'sbatch-submit_icon-resize-raw.sh',
            f'{tempdir}/resize-raw.ipynb'
            ])
        #break