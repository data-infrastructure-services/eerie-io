#!/bin/bash

#SBATCH --job-name=ICON_SI_cmor  
#SBATCH --partition=compute
#SBATCH --time=8:00:00
#SBATCH --mem=256GB
#SBATCH --mail-type=FAIL       
#SBATCH --account=bm1344       
#SBATCH --output=ICON_cmor.o%j    
#SBATCH --error=ICON_cmor.e%j 

cdo="/work/bm0021/cdo_incl_cmor/cdo-test_cmortest_gcc/bin/cdo"
config="./blub.txt"
export HDF5_PLUGIN_PATH=/fastdata/k20200/k202186/public/hdf5/plugins/
module load netcdf-c

initskip="1950"
exp="erc2002"
model="ICON"
baseurl="https://eerie.cloud.dkrz.de/cmor/datasets"
ds="icon-esm-er.eerie-control-1950.v20240618.ocean.native.2d_monthly_mean"

outdirtrunk="/work/bm1344/k202193/Regrid/atmos_regrid"
mkdir -p $outdirtrunk

targetgrid=/work/mh0256/m300466/DPP/grid/temp_IFS25invertlat_MR_gridexample.nc
srcgrid=/pool/data/ICON/grids/public/mpim/0016/icon_grid_0016_R02B09_O.nc
remapwgt=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_IFS25invertlat_yconremapweights_lsm.nc
lsm=/work/bm1344/DKRZ/ICON/erc1011/postprocessing/interpolation/r2b9O_lsm.nc


pattern="2d_monthly_mean"  
outdir="${outdirtrunk}/${pattern}_remap025"
outpattern="${pattern}_remap025"

declare -A variables
variables=(
  [heat_content_snow]="sisnhc"
  [heat_content_seaice]="sihc"
  [Qbot]="siflcondbot"  
  #[hs]="sisnthick" 
)

total_timesteps=384
chunk_size=12
maxjobs=10 

process_chunk() {
  local var="$1"
  local cn="$2"
  local start_time="$3"
  local end_time="$4"


  selection="-select,name=$var,timestep=$start_time/$end_time"

  $cdo -L -P 16 cmor,SImon,i=$config,cn=$cn,n=$var -setattribute,source_id=ICON-ESM-ER,grid_label=gr,grid=gr025 -remap,${targetgrid},${remapwgt} -div -setgrid,${srcgrid} [ $selection $baseurl/$ds/zarr\#mode\=zarr,s3,consolidated ] $lsm
}


export -f process_chunk  
export cdo config targetgrid remapwgt srcgrid baseurl ds lsm total_timesteps chunk_size  

generate_chunks() {
  local var="$1"
  local cn="$2"
  local start_time=1
  while [ $start_time -le $total_timesteps ]; do
    local end_time=$((start_time + chunk_size - 1))
    if [ $end_time -gt $total_timesteps ]; then
      end_time=$total_timesteps
    fi
    echo "$var $cn $start_time $end_time"
    start_time=$((end_time + 1))
  done
}

for var in "${!variables[@]}"; do
  cn="${variables[$var]}"
  if [ -z "$cn" ]; then
    echo "Skipping unsupported variable: $var"
    continue
  fi
  generate_chunks "$var" "$cn"
done | xargs -n 4 -P $maxjobs bash -c 'process_chunk "$@"' _


