#!/bin/bash

#SBATCH --job-name=CMOR_FESOM     
#SBATCH --partition=shared
#SBATCH --time=0-08:00:00
#SBATCH --mem=100GB
#SBATCH --mail-type=FAIL       
#SBATCH --account=bm1344       
#SBATCH --output=CMOR.o%j    
#SBATCH --error=CMOR.e%j 


baseurl="https://eerie.cloud.dkrz.de/cmor/datasets"
ds="ifs-fesom2-sr.hist-1950.v20240304.ocean.gr025.2D_monthly_avg"
cdo="/work/bm0021/cdo_incl_cmor/cdo-test_cmortest_gcc/bin/cdo"
config="/work/bm1344/DKRZ/intake_catalogues/dkrz/disk/attributes/ifs-fesom2-sr/hist-1950/v20240304/ocean/blub.txt"
export HDF5_PLUGIN_PATH=/fastdata/k20200/k202186/public/hdf5/plugins/


declare -A variables
variables=(
  [avg_siconc]="siconc"
  [avg_sisnthick]="sisnthick"
  [avg_sithick]="sithick"
  [avg_siue]="siu"
  [avg_sivn]="siv"
)

# Total timesteps in the dataset
total_timesteps=240

# time Chunk size for a file
chunk_size=12

for var in "${!variables[@]}"; do
  cn="${variables[$var]}"
  
  if [ -z "$cn" ]; then
    echo "Skipping unsupported variable: $var"
    continue
  fi

  unit_option=""
  if [ "$var" == "avg_siconc" ]; then
    unit_option=",u=%"  
  fi

  start_time=1
  while [ $start_time -le $total_timesteps ]; do
    end_time=$((start_time + chunk_size - 1))
    if [ $end_time -gt $total_timesteps ]; then
      end_time=$total_timesteps
    fi

    selection="-select,name=$var,timestep=$start_time/$end_time"

    echo "Processing $var from timestep $start_time to $end_time"
    echo "$cdo cmor,SImon,i=$config,cn=$cn,n=$var$unit_option $selection $baseurl/$ds/zarr#mode=zarr,s3,consolidated"
    
    $cdo cmor,SImon,i=$config,cn=$cn,n=$var$unit_option -setmissval,1e20 -setmisstoc,1e20 -setmissval,NaNf $selection $baseurl/$ds/zarr\#mode\=zarr,s3,consolidated
    
    start_time=$((end_time + 1))
  done
done





