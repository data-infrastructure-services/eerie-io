#!/bin/bash
#SBATCH --partition=shared
#SBATCH --time=30:00:00
#SBATCH --mem=16G
#SBATCH --output=%x_%j.log
#SBATCH --account=bm0021
#SBATCH --qos=esgf
#SBATCH --begin=now

source activate /work/bm0021/conda-envs/eerie_io
jupyter nbconvert --execute --to html $1

