#!/bin/ksh -l
#SBATCH --job-name=slk-archive
#SBATCH --partition=compute
#SBATCH --time=48:00:00
#SBATCH --mem=200G
#SBATCH --output=%x_%j.log
#SBATCH --account=bm0021
#SBATCH --qos=esgf
#SBATCH --begin=now #

source activate /work/bm0021/conda-envs/eerie_io
jupyter nbconvert --execute --to html $1

sed -i 's\--begin=now #\--begin=now+96hours\g' sbatch-submit_icon-outdata.sh 
sbatch sbatch-submit_icon-outdata.sh
