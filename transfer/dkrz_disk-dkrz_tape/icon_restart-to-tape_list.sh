#!/bin/bash
RUN="/work/bm1344/k203123/icon-mpim/ec1-2/experiments/erc1007/scripts/erc1007.run"
FREQ=5
OUTLIST="restart-dirs-to-tape"
rm $OUTLIST*
RUN=$1
FREQ=$2

run_dir_trunk=$(grep 'EXPDIR=' $RUN | cut -d '=' -f 2 | rev | cut -d '/' -f 2- | rev);
echo "look in $run_dir_trunk"
restart_years=$(find $run_dir_trunk/restarts/ -mindepth 1 -maxdepth 1 | rev | cut -d '_' -f 1 | rev | sort | cut -d 'T' -f 1 | cut -c 1-4 | sort -u);

first_year=$(echo $restart_years| cut -d ' ' -f 1)
recent_year=$(echo $restart_years| rev | cut -d ' ' -f 1 | rev)

for year in $(seq $first_year $FREQ $recent_year); do
    find $run_dir_trunk/restarts/*restart_atm*${year}01* -maxdepth 0 >>${OUTLIST}-atm.txt
    find $run_dir_trunk/restarts/*restart_oce*${year}01* -maxdepth 0 >>${OUTLIST}-oce.txt
done
#EXPDIR_STRING = r"EXPDIRs*=\s*(\S+)"
