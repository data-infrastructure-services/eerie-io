#!/bin/bash
module load packems

target_gigabyte=20
parallel_jobs=2
temporary_tar_disk="/scratch/k/k204210/"
tape_path="/arch/bm1344/ICON/restart/"
archive="-a" #+a only creates tars
purge="-p" #NEVER use -P

rm packems_*.log
OUTLIST="restart-dirs-to-tape"
SUBMODELS="atm oce"

packems_cmd="packems \
            -d $temporary_tar_disk \
            $archive \
            -j ${parallel_jobs} \
            -t ${target_gigabyte} \
            -L --lock-break"
echo "Final packems command options:"
echo $packems_cmd

iteration=1
for submodel in $SUBMODELS; do
    len=$(cat ${OUTLIST}-${submodel}.txt | wc -l);
    for dir in $(cat ${OUTLIST}-${submodel}.txt); do
        echo "#### Start packems for directory $dir ";
        echo "$iteration / $len" ;
        dirname=$(echo $dir | rev | cut -d '/' -f 1 | rev);
        expname=$(echo $dirname | cut -d '_' -f 1);
        #
        $packems_cmd -o $dirname -S ${tape_path}/${expname} $dir >packems_${iteration}.log 2>&1 &
        #
        iteration=$((iteration+1)) ;
    done;
    wait
done