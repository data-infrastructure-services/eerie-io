#!/bin/bash
#
# PACKEMS + slk
#
#SBATCH --job-name=packems-icon-restart
#SBATCH --partition=compute
#SBATCH --time=48:00:00
#SBATCH --mem=200G
#SBATCH --output=%x_%j.log
#SBATCH --account=bm0021
#SBATCH --qos=esgf

FREQ=5
./icon_restart-to-tape_list.sh \
    /work/bm1344/k203123/icon-mpim/ec1-2/experiments/erc1007/scripts/erc1007.run \
    $FREQ
./icon_restart-to-tape_packems.sh