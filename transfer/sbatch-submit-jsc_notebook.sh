#!/bin/bash

#SBATCH -J dask-worker
#SBATCH -p devel
#SBATCH -A hhb19
#SBATCH --cpus-per-task=1
#SBATCH --mem=8G
#SBATCH -t 00:10:00
#SBATCH -J eerieio
#SBATCH --output=LOG_EERIE-io.%j.o
#SBATCH --begin=now+24hours

activate="/p/project/chhb19/wachsmann1/miniconda/bin/activate"
env="/p/project/chhb19/wachsmann1/miniconda/envs/mambaenv/envs/eerie_io"
source  ${activate} $env
jupyter nbconvert --execute --to notebook --inplace $1
sbatch sbatch-submit-jsc.sh $1