#!/bin/bash
#SBATCH -J rsync-scratchtotape
#SBATCH -p shared
#SBATCH -A bm0021
#SBATCH -n 20
#SBATCH --cpus-per-task=10
#SBATCH -t 48:00:00
#SBATCH --output=log_rsync.%j.o
#SBATCH --error=log_rsync.%j.o

#rsyncs all directories in source_dir (maxdepth 1 ) to dest_dir with X parallel_processes

 source_dir=$1
 dest_dir=$2
 parallel_processes=$3
 ftype=$4
 searchpattern=$5
 
 echo "xargs -n1 -P${parallel_processes} -I% \
   rsync -rPav % ${dest_dir}/% "
 find $source_dir -mindepth 1 -maxdepth 1 -type ${ftype} ${searchpattern} -printf '%P\n' | \
   xargs -n1 -P${parallel_processes} -I% \
   rsync -rPav ${source_dir}/% ${dest_dir}/ 
