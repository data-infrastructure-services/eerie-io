#!/bin/ksh -l

#SBATCH -J dask-worker
#SBATCH -p shared
#SBATCH -A bm0021
#SBATCH --cpus-per-task=1
#SBATCH --mem=10G
#SBATCH -t 48:00:00
#SBATCH -J tapetoscratch
#SBATCH --output=/work/bm1344/k204210/LOG_EERIE_dkrz-tape-to-dkrz.%j.o
#SBATCH --output=/work/bm1344/k204210/LOG_EERIE_dkrz-tape-to-dkrz.%j.o

module load python3

jupyter nbconvert --execute --to html $1
