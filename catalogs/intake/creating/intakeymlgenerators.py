import pandas as pd
import os
def create_awi_raw(sources : dict,k : str, df:pd.DataFrame):
    sources["sources"][k]={
        "parameters":dict(
            var=dict(
                description= 'variable_id',
                type="str",
                default='ssh',
                allowed=[v for v in df["variable_id"].unique()]
            )
        )
    }
    sources["sources"][k].update(
        dict(
            driver="netcdf",
            args=dict(
                urlpath=os.path.join(
                    os.path.dirname(
                        df["uri"].values[0]
                    ),
                    "{{var}}.*.nc"
                ),
                xarray_kwargs=dict(
                    coords="minimal",
                    compat="override",
                    decode_times="False"
                )
            )
        )
    )
    return sources