import os.path as osp

def map_icon_ngc(path):
    fileparts={}
    fileparts["uri"]=path
    filename='.'.join(osp.basename(path).split('.')[0:-1])
    fileparts["filename"]=filename
    filecut=filename.split('_')
    if len(filecut)<4:
        return fileparts
    fileparts["simulation_id"]=filecut[0]
    fileparts["realm"]=filecut[1]
    if len(filecut)>4:
        fileparts["level_type"]=filecut[2]
        fileparts["frequency"]=filecut[3]
        fileparts["time_reduction"]=filecut[4]
        fileparts["time_min"]=filecut[5]
        if fileparts["level_type"]=="moc":
            fileparts["level_type"]="ml"
        if fileparts["realm"]=="oce" and fileparts["level_type"]=="2d":
            fileparts["level_type"]="ml"
        if fileparts["realm"]=="lnd" and fileparts["level_type"]=="2d":
            fileparts["level_type"]="ml"
        if fileparts["realm"]=="atm" and fileparts["level_type"]=="2d":
            fileparts["level_type"]="ml"

    elif len(filecut) == 4:
        fileparts["frequency"]=filecut[2]
        fileparts["time_reduction"]="mean"
        fileparts["time_min"]=filecut[3]
        fileparts["level_type"]="ml"
    else:
        print(path)
    if fileparts["frequency"]=="1h":
        fileparts["frequency"]="1hour"
    elif fileparts["frequency"]=="3h":
        fileparts["frequency"]="3hour"
    elif fileparts["frequency"]=="23h":
        fileparts["frequency"]="23hour"
    elif fileparts["frequency"]=="1d":
        fileparts["frequency"]="1day"
    elif fileparts["frequency"]=="1mth" or fileparts["frequency"]=="mon" :
        fileparts["frequency"]="1month"

    return fileparts

def map_awi_ng5(path):
    import fsspec
    fileparts={}
    fileparts["uri"]=path
    fs=fsspec.get_mapper(path).fs
    fileparts["size"]=fs.du(path)
    fs.created(path).strftime("%Y-%m-%d")
    filename='.'.join(osp.basename(path).split('.')[0:-1])
    filecut=filename.split('.')
    if len(filecut)!=3:
        print(path)
        return fileparts
    fileparts["variable_id"]=filecut[0]
    submodel=filecut[1]
    if submodel == "fesom":
        fileparts["realm"]="oce"
    fileparts["time_min"]=filecut[2]
    return fileparts
    