import requests
import sys
import copy
import xarray as xr
import fsspec
import yaml
import glob
import tqdm
import os

KERCHUNK_TRUNK="/work/bm1344/DKRZ/kerchunks_batched"
exp="eerie-control-1950"
EXPERIMENT="ifs-fesom2-sr_eerie-control-1950"
#EXPERIMENT=exp
MODEL="IFS-FESOM2-SR"
#MODEL="ICON-ESM-ER"
l_ICON=False

EXP_NAMES=["erc1011"]
if not l_ICON:
    print(KERCHUNK_TRUNK+"/"+MODEL.lower()+"_"+exp+"*")
    EXP_NAMES=[a.split('/')[-1] for a in glob.glob(KERCHUNK_TRUNK+"/"+MODEL+"_"+exp+"*")]
    print("The following experiments are intaken:")
    print(EXP_NAMES)

dset_idx=0
#dset_idx=-2
realm_mapping={
    "icon":[("atmos","atm"), ("land","lnd"), ("ocean","oce")],
    "fesom":[("atmos","IFS_"), ("ocean","FESOM_")]
}
gridlabel_mapping={
    "icon":[("native","remap025"),("gr025","remap025"),("gr100","remap100")],
    "fesom":[("native","native"),("gr025","025")]
}
INTAKE_FLAT="/work/bm1344/DKRZ/intake/dkrz_eerie_esm.yaml"

def create_or_update(intfile, ss):
    if os.path.isfile(intfile):
        with fsspec.open(intfile,"r") as f:
            cat=yaml.full_load(f,)
            cat["sources"].update(ss["sources"])
        with fsspec.open(intfile,"w") as f:
            yaml.dump(cat,f)
    else:
        with fsspec.open(intfile,"w") as f:
            yaml.dump(ss,f)
            
def create_or_update2(intfile, ss):
    with fsspec.open(intfile,"w") as f:
        yaml.dump(ss,f)

for EXP_NAME in EXP_NAMES:
    kerchunk_dir=f"{KERCHUNK_TRUNK}/{EXP_NAME}"
    kerchunk_jsons=[kerchunk_dir +"/"+a for a in os.listdir(kerchunk_dir)]

    intake_dict = dict(
        sources=dict(
            entry=dict(
                args=dict(
                    urlpath="reference::/",
                    consolidated= False,
                    chunks="auto",
                    storage_options=dict(
                        remote_protocol="file", 
                    )
                ),
                driver="zarr",
                metadata=dict(
                    simulation_id=EXP_NAME,
                    source_id=MODEL,
                    experiment=EXPERIMENT,
                    plots=dict(
                        quicklook=dict(
                            kind="image",
                            use_dask=True,
                            groupby="time",
                            cmap="jet",
                            x="lon",
                            y="lat",
                            z="{{variables}}",
                            geo=True,
                            coastline="50m",
                            width=800,
                            aspect=1
                        ),
                    )
                ),
                parameters=dict(
                    variables=dict(
                        type="str",
                        description="All available variables in the dataset",
                        default=None,
                        allowed=None
                    )
                )
            )
        )
    )

    for kerchunk_file in tqdm.tqdm(kerchunk_jsons):
        #if "FESOM_025_daily" in kerchunk_file:
        #    continue
        print(kerchunk_file)
        if kerchunk_file.endswith('.nc') or kerchunk_file.endswith('.json'):
            continue
        ds=xr.open_zarr(
            "reference::/"+kerchunk_file+"/combined.parq",
            consolidated= False,
            storage_options=dict(
                remote_protocol="file", 
            ),
            chunks="auto"
        )
        dsetexp=EXP_NAME
        #dsetsource=MODEL
        if exp:
            dsetexp=exp
        elif "experiment_id" in ds.attrs:
            dsetexp=ds.attrs["experiment_id"]
        #if "source_id" in ds.attrs:
        #    dsetexp=ds.attrs["source_id"]

        dset = None
        if type(kerchunk_file) == list:
            dset = kerchunk_file[0].split('/')[-1].split('.')[dset_idx]
        else:
            dset=kerchunk_file.split('/')[-1].split('.')[dset_idx]
        dset=f"{MODEL}_{dsetexp}_{dset}"

        intake_dict["sources"][dset] = copy.deepcopy(intake_dict["sources"]["entry"])
        intake_dict["sources"][dset]["args"]["urlpath"] += kerchunk_file+"/combined.parq"
        if "fesom.json" in dset:
            del intake_dict["sources"][dset]["args"]["chunks"]        

        for k,v in ds.attrs.items():
            intake_dict["sources"][dset]["metadata"][k]=v
        varlist=list(ds.data_vars)
        long_names=[]
        for var in varlist:
            if "long_name" in ds[var].attrs:
                long_names.append(ds[var].attrs["long_name"])
        intake_dict["sources"][dset]["parameters"]["variables"]["allowed"]=varlist
        intake_dict["sources"][dset]["parameters"]["variables"]["default"]=varlist[0]
        
        if not "remap" in dset and not "025" in dset:
            del intake_dict["sources"][dset]["metadata"]["plots"]
        else:
            idx=0
            idx_fb=0
            while idx < len(varlist):
                remaining=list(set(ds[varlist[idx]].dims)-set(["lat","lon","time"]))
                if "bnds" in varlist[idx] or "wa" == varlist[idx]:
                    idx+=1
                elif (len(ds[varlist[idx]].squeeze().dims))>3:
                    if len(remaining) == 1:
                        idx_fb=idx
                    idx+=1
                else:
                    break
            if idx == len(varlist):
                if idx_fb > 0:
                    intake_dict["sources"][dset]["metadata"]["plots"]["quicklook"]["groupby"]=["time",remaining[0]]
                    plotvar=varlist[idx_fb]
                    intake_dict["sources"][dset]["parameters"]["variables"]["default"]=plotvar
                    #minval=round(ds[plotvar].isel(time=0).min().load().values[()].item(),3)
                    #maxval=round(ds[plotvar].isel(time=0).max().load().values[()].item(),3)
                    #intake_dict["sources"][dset]["metadata"]["plots"]["quicklook"]["clim"]=(minval,maxval)
                else:
                    del intake_dict["sources"][dset]["metadata"]["plots"]
            else:       
                plotvar=varlist[idx]
                intake_dict["sources"][dset]["parameters"]["variables"]["default"]=plotvar
                #minval=round(ds[plotvar].isel(time=0).min().load().values[()].item(),3)
                #maxval=round(ds[plotvar].isel(time=0).max().load().values[()].item(),3)
                #intake_dict["sources"][dset]["metadata"]["plots"]["quicklook"]["clim"]=(minval,maxval)
        intake_dict["sources"][dset]["metadata"]["variable-long_names"]=long_names
        ds.close()            

    del intake_dict["sources"]["entry"]

    cat=None

    model_realm_mapping=realm_mapping["fesom"]
    model_gls_mapping=gridlabel_mapping["fesom"]
    if l_ICON:
        model_realm_mapping=realm_mapping["icon"]
        model_gls_mapping=gridlabel_mapping["icon"]
        
    for realm in model_realm_mapping:
        for gl in model_gls_mapping:
            INTAKE_FILE = f"/work/bm1344/DKRZ/intake_catalogues/dkrz/disk/model-output/{MODEL.lower()}/{exp}/{realm[0]}/{gl[0]}/main.yaml"
            subset = copy.deepcopy(intake_dict)
            print(realm[0],gl[0])
            for s in intake_dict["sources"]:
                print(s)
                if not realm[1] in s:
                    del subset["sources"][s]
                    continue
                if l_ICON:
                    if gl[0] == "native":
                        if "remap" in s:
                            del subset["sources"][s]
                            continue
                    else:
                        if not gl[1] in s:
                            del subset["sources"][s]
                            continue   
                else:
                    if not gl[1] in s:
                        del subset["sources"][s]
                        continue   
                news='_'.join(s.split('_')[3:])
                if l_ICON:
                    news=news.replace("_1d_","_daily_").replace("ml_","model-level_").replace("1mth","monthly").replace("_remap025","")
                else:
                    news='_'.join(s.split('_')[4:])
                    #news=news.replace("_3d","")
                print("survived:")
                print(news)
                subset["sources"][news]=copy.deepcopy(intake_dict["sources"][s])
                del subset["sources"][s]
            if subset["sources"]:
                create_or_update(INTAKE_FILE, subset)
                
    create_or_update(INTAKE_FLAT, intake_dict)