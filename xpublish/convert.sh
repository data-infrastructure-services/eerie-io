jupyter nbconvert --to python netcdf_downloader.ipynb
jupyter nbconvert --to python eeriecloud_tabulator.ipynb
panel convert netcdf_downloader.py eeriecloud_tabulator.py --to pyodide-worker --out ncdownloader --pwa --title DKRZ_ABC --index --num-procs 4 --compiled
