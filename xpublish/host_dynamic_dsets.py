import os, glob
import xarray as xr
import pickle
import fsspec as fs
from pathlib import Path
import xpublish as xp
from xpublish import Plugin, hookimpl, Dependencies
from xpublish.dependencies import get_dataset
from typing import Sequence
from fastapi import APIRouter
import intake
import tqdm
import numcodecs
import io
from copy import deepcopy as copy
import requests
import gc
import asyncio
import hvplot.xarray
import nest_asyncio
nest_asyncio.apply()
from intake.config import conf
conf['cache_disabled'] = True

#CATALOG_FILE="/work/bm1344/DKRZ/intake/dkrz_eerie_esm.yaml"
CATALOG_FILE="/work/bm1344/DKRZ/intake_catalogues/dkrz/disk/main.yaml"
PICKLE_TRUNK="/work/bm1344/DKRZ/pickle/"
ADDRESS="tcp://127.0.0.1:42577"

from starlette.responses import StreamingResponse
from fastapi import FastAPI, APIRouter
from fastapi import HTTPException, Depends, Request
from fastapi.responses import FileResponse, HTMLResponse,RedirectResponse
#from fastapi.templating import Jinja2Templates
from pathlib import Path
import holoviews as hv
import panel as pn 
#from bokeh.embed import server_document
import numpy as np

# Mount a directory as a static route on the file router
#from starlette.staticfiles import StaticFiles
#file_router.mount("/static", StaticFiles(directory="/work/bm1344/DKRZ/ICON/erc1011/postprocessing"), name="static")

#templates = Jinja2Templates(directory="/work/bm1344/DKRZ/intake/apps/templates")
def compress_data(partds):
    import numcodecs
    rounding = numcodecs.BitRound(keepbits=12)
    return rounding.decode(rounding.encode(partds))

def find_data_sources(catalog,name=None):
    newname='.'.join(
        [ a 
         for a in [name, catalog.name]
         if a
        ]
    )
    data_sources = []

    for key, entry in catalog.items():
        if isinstance(entry,intake.source.csv.CSVSource):
            continue
        if isinstance(entry, intake.catalog.Catalog):
            if newname == "main":
                newname = None
            # If the entry is a subcatalog, recursively search it
            data_sources.extend(find_data_sources(entry, newname))
        elif isinstance(entry, intake.source.base.DataSource):
            data_sources.append(newname+"."+key)

    return data_sources


class PlotPlugin(Plugin):
    name: str = 'PlotPlugin'

    dataset_router_prefix: str = '/plot'
    dataset_router_tags: Sequence[str] = ['plot']

    hvplotopts : dict = dict(
                use_dask=True,
                width=1600,
                x="lon",
                coastline="50m",
                y="lat",
                levels=20,
                widget_type="scrubber",
                widget_location="bottom"
                )


    def drop_noplot_vars(self,ds,coords):
        for dropvar in ["cell_sea_land_mask","coast","bnds"]:
            if dropvar in ds.coords or dropvar in ds.dims:
                ds=ds.drop(dropvar)
        keeplist=["lat","lon","time"]
        if coords:
            keeplist+=list(coords.keys())
        droppable=list(set(ds.coords)-set(keeplist))
        if len(droppable) >0:
            for s in droppable:
                if s not in ds.dims:
                    ds=ds.drop(s)
                else:
                    ds=ds.isel({s:0})
        return ds

    def change_units(self,ds):
        if "lat" in ds.coords and "lat" not in ds.dims and ds["lat"].attrs["units"] != "degrees":
            for c in ["lat","lon"]:
                ds[c]=xr.apply_ufunc(np.rad2deg,ds[c],dask="parallelized")
                ds[c].attrs["units"]="degrees"
        return ds

    def prepare_coords(self,ds,var_name,coords):
        coords_to_load=None
        if not coords:
#            if not "time" in ds[var_name].dims:
#                del ds
#                ds=None
#            else:
            if "time" in ds[var_name].dims:
                ds=ds.isel({"time":range(-12,-1)})
        else:
            if "time" in coords.keys():
                ds = ds.isel(time=range(coords["time"][0],coords["time"][1]))
                del coords["time"]
            coords_to_load=[
                    a
                    for a in coords.keys()
                    if ds[a].chunks != None
                    ]
        return ds,coords_to_load

    async def prepare_ds(self,dataset,var_name,coords):
        ds=dataset.copy(deep=True)
        client = await get_dask_client()
        if var_name not in ds.variables:
            await client.close()
            raise HTTPException(status_code=404, detail=f"Variable '{var_name}' not found in dataset")
        ds=ds[[var_name]]
        ds=self.drop_noplot_vars(ds,coords)
        ds=self.change_units(ds)
        ds,coords_to_load = self.prepare_coords(ds,var_name,coords)
        #if not ds:
        #    await client.close()
        #    raise HTTPException(status_code=404, detail=f"Could not subset. Please specify range.")
        if coords_to_load:
            dscoords = ds[coords_to_load] #.reset_coords()[coords_to_load]
            dscoords = client.persist(dscoords)
            dscoords = await client.compute(dscoords)
            for a in coords_to_load:
                ds[a]=dscoords[a]
        if coords:
            for coord,rangeval in coords.items():
                if coord != "time":
                    ds = ds.where(
                            (
                                (ds[coord] >= rangeval[0])
                                &(ds[coord] < rangeval[1])
                                ), drop=True
                            )
        print("size")
        print(ds.nbytes)
        if ds[var_name].size == 0:
            await client.close()
            raise HTTPException(status_code=404, detail=f"Your subset leads to empty array.")

#        droppable=list(set(ds.squeeze().dims)-set(["time","lat","lon","ncells","ncells2"])-set(coords.keys()))#
                #                ds=ds.drop(droppable)
#        ds=ds.squeeze()
        if ds.nbytes > 1024*1024*1024 :
            await client.close()
            raise HTTPException(status_code=404, detail="Dataset too large after subsetting")
        ds=client.persist(ds[var_name])
        ds= await client.compute(ds)
        mima = [ds.min(),ds.max()]
#        mima = [await client.compute(ds.min()),await client.compute(ds.max())]
        cmin,cmax=round(mima[0].data.item(),6),round(mima[1].data.item(),6)
        await client.close()
        return ds, (cmin,cmax)

    async def gen_plot(self, dataset, var_name, coords=None, kind="image",cmap="jet"):
        from dask.distributed import Client
        zarraddress=os.environ["ZARR_ADDRESS"]
        ds,clims = await self.prepare_ds(dataset,var_name,coords)
        print(ds)
        print(clims)
        plot,html_bytes = None,None
        with Client(zarraddress) as genplot_client:
            html_bytes = io.BytesIO()
            if "lon" not in ds.dims:
                ds["lon"]=xr.where(ds["lon"]>180.0,ds["lon"]-360.,ds["lon"])
            #plot = await client.submit(ds.hvplot,
            #plot = client.compute(ds.hvplot,#(
                plot = ds.hvplot(
                    c=var_name,
                    kind="scatter",
                    hover=False,
                    s=50,
                    clim=clims,
                    cmap=cmap,
                    rasterize=False,
                    **self.hvplotopts
                    )
#            plot = plot.result()

                plot=pn.panel(plot)#.servable()
            else:
#            plot = await client.submit(ds.hvplot,
 #           plot= client.compute(ds.hvplot,
                plot=ds.hvplot(
                    c=var_name,
                    kind=kind,
                    clim=clims,
                    cmap=cmap,
                    rasterize=True,
                    project=True,
                    **self.hvplotopts
                    )
#            plot = plot.result()
            plot.save(html_bytes, fmt='html',embed=True)
            del plot
        return html_bytes

    @hookimpl
    def dataset_router(self, deps: Dependencies):
        router = APIRouter(prefix=self.dataset_router_prefix, tags=list(self.dataset_router_tags))

        @router.get('/{var_name}/{kind}/{cmap}/{selection:path}', response_class=StreamingResponse)
        async def get_custom_plot(var_name: str, kind:str, cmap:str, request: Request, selection:str =  None, dataset: xr.Dataset = Depends(get_dataset)):
            if var_name not in dataset.variables:
                raise HTTPException(status_code=404, detail=f"Variable '{var_name}' not found in dataset")
            if kind not in dataset.hvplot.__all__ :
                raise HTTPException(status_code=404, detail=f"Cannot use '{kind}' for plotting")
            if cmap not in hv.plotting.list_cmaps():
                raise HTTPException(status_code=404, detail=f"Variable '{camp}' not found in colormaps")
            coords=None
            if selection:
                rawcoords=selection.split('/')
                if len(rawcoords)%2 != 0:
                    raise HTTPException(status_code=404, detail=f"Variable '{rawcoords}' must have length 4")
                coords=dict()
                for idx,(coord, coord_range) in enumerate(zip(rawcoords[:-1], rawcoords[1:])):
                    if idx % 2 != 0:
                        continue
                    if coord not in dataset[var_name].coords:
                        raise HTTPException(status_code=404, detail=f"Coordinate '{coord}' not found in dataset")
                    try:
                        l=coord_range.split('_')[0]
                        h=coord_range.split('_')[1]
#                        if coord in ["lat","lon","year","month","day"]:
                        l=int(l)
                        h=int(h)
                        coords[coord]=[l,h]
                    except Exception as e:
                        print(e)
                        raise HTTPException(status_code=404, detail=f"Could not use range {coord_range}. Must be separated by '_' and of type integer.")
            
            #script = server_document('http://127.0.0.1:7000/app')
            #plot = self.gen_plot(dataset,var_name,kind=kind,cmap=cmap,coords=coords)
            #pn.serve(
            #        {
            #            '/app': plot.servable()
            #            },
            #        port=7000,
            #        allow_websocket_origin=["127.0.0.1:9000"],
            #        address="127.0.0.1",
            #        show=False
            #        )
            #return templates.TemplateResponse("base.html", {"request": request, "script": script})
            html_bytes = asyncio.get_event_loop().run_until_complete(self.gen_plot(dataset,var_name,kind=kind,cmap=cmap,coords=coords))
#            html_bytes= await self.gen_plot(dataset,var_name,kind=kind,cmap=cmap,coords=coords)

            html_bytes.seek(0)
            return StreamingResponse(html_bytes, media_type="text/html")
        return router

class FileServe(Plugin):
    name: str = 'FileServe'

    app_router_prefix: str = '/static'
    app_router_tags: Sequence[str] = ['get_file']

    @hookimpl
    def app_router(self, deps: Dependencies):
        router = APIRouter(prefix=self.app_router_prefix, tags=list(self.app_router_tags))

        @router.get('/{staticfile:path}')
        async def get_partial_file(staticfile: str, request: Request):
            trunk="/work/bm1344/DKRZ"
            file_path=os.path.join(trunk, staticfile)
            if not os.path.isfile(file_path):
                return None

            range_header = request.headers.get("Range")
            if not range_header or not range_header.startswith("bytes="):
                # Handle the case where the Range header is missing or invalid
                # You can return a full file in this case or respond with an error
                return FileResponse(file_path)

            range_values = range_header.replace("bytes=", "").split("-")
            if len(range_values) != 2:
                # Handle an invalid range header
                return FileResponse(file_path)

            start_byte, end_byte = map(int, range_values)
            file_size = os.path.getsize(file_path)

            if end_byte >= file_size:
                end_byte = file_size - 1

            content_length = end_byte - start_byte + 1

            headers = {
                "Content-Range": f"bytes {start_byte}-{end_byte}/{file_size}",
                "Content-Length": str(content_length),
                "Accept-Ranges": "bytes",
            }
            status_code = 206  # Partial Content

            fss = fs.filesystem("file")
            file_handle = fss.open(file_path, "rb")
            file_handle.seek(start_byte)

            def file_generator():
                remaining_bytes = content_length
                chunk_size = 4096
                while remaining_bytes > 0:
                    data = file_handle.read(min(remaining_bytes, chunk_size))
                    if not data:
                        break
                    yield data
                    remaining_bytes -= len(data)

            return StreamingResponse(file_generator(), headers=headers, status_code=status_code, media_type="application/octet-stream")

        return router

def list_pickles(path):
    file_list = []

    for root, _, files in os.walk(path):
        for file in files:
            file_path = os.path.join(root, file)
            file_list.append(file_path)

    return file_list

class DynamicAdd(Plugin):
    name : str= "dynamic-add"

    dataset_router_prefix: str = '/groupby'
    dataset_router_tags: Sequence[str] = ['groupby']

    @hookimpl
    def dataset_router(self, deps: Dependencies):
        router = APIRouter(prefix=self.dataset_router_prefix, tags=list(self.dataset_router_tags))

        @router.get('/{var_name}/{groupby_coord}/{groupby_action}')
        def eval_groupby(var_name: str, groupby_coord:str, groupby_action:str, request: Request, dataset: xr.Dataset = Depends(get_dataset)):
            if var_name not in dataset.variables:
                raise HTTPException(status_code=404, detail=f"Variable '{var_name}' not found in dataset")
            if groupby_coord.split('.')[0] not in dataset.dims and groupby_coord.split('.')[0] not in dataset.coords:
                raise HTTPException(status_code=404, detail=f"Groupby_coord '{groupby_coord}' not found in dataset")
            if not groupby_action in ["mean","min","max","count","first","last","sum"]:
                raise HTTPException(status_code=404, detail=f"Did not find groupby action '{groupby_action}'")
            evalstring=f"dataset['{var_name}'].groupby('{groupby_coord}').{groupby_action}()"
            ds= eval(evalstring)
            newvar=f'{var_name}_{groupby_coord}_{groupby_action}'
            if not newvar in dataset:
                evalstring=f"dataset['pp_{newvar}']=dataset['{var_name}'].groupby('{groupby_coord}').{groupby_action}()"
                exec(evalstring)
            with xr.set_options(display_style='html'):
                return HTMLResponse(ds._repr_html_())

        return router

class DynamicKerchunk(Plugin):
    name : str = "dynamic-kerchunk"
       
    @hookimpl
    def get_datasets(self):
        global dsdict
        return list(dsdict.keys())

    @hookimpl
    def get_dataset(self, dataset_id: str):
        global dsdict
        #from dask.distributed import get_client
        #local_client=get_client()
        if dataset_id not in dsdict:
            raise HTTPException(status_code=404, detail=f"Could not find {dataset_id}")
        return dsdict[dataset_id]


#    @hookimpl
    def get_datasets2(self):
        cat = intake.open_catalog(CATALOG_FILE)
        dsets = list(cat)
        pickle_abs = list_pickles(PICKLE_TRUNK)
        pickle_list = ['_'.join(a.split('/')[-2:]) for a in pickle_abs]
        pickle_list = ['.'.join(a.split('.')[:-1]) for a in pickle_list]
        dsets+=pickle_list
        dsetsnew = [a+"_12bit" for a in dsets if not "remap" in a and not "025" in a]
        dsets_compr = [a for a in dsets if a+"_12bit" not in dsetsnew]
        dsetsnew+=dsets_compr
        dsetsnew.sort()
        return dsetsnew

#    @hookimpl
    def get_dataset2(self, dataset_id: str):
        lcompress = False
        if dataset_id.endswith("_12bit"):
            lcompress = True
            dataset_id = dataset_id.replace("_12bit","")
            
        cat = intake.open_catalog(CATALOG_FILE)
        dsets = list(cat)
        pickle_abs = list_pickles(PICKLE_TRUNK)
        pickle_list = ['_'.join(a.split('/')[-2:]) for a in pickle_abs]
        pickle_list = ['.'.join(a.split('.')[:-1]) for a in pickle_list]
        if dataset_id in dsets or any([dataset_id in a for a in pickle_list]):
            if dataset_id in dsets:
                ds = cat[dataset_id].to_dask()
            else:
                fn=pickle_abs[pickle_list.index(dataset_id)]
                ds = pickle.load(
                        fs.open(
                            fn
                            ).open()
                        )
                #if "fesom" in dsid
                #apply_ufunc
                #set encoding

            return ds.squeeze(drop=True)

        return None

async def get_dask_client():
    from dask.distributed import Client
    return await Client(
            asynchronous=True,
            processes=True,
            n_workers=2,
            threads_per_worker=4,
            memory_limit="4GB",
            set_as_default=False
            )

async def get_dask_cluster():
    from dask.distributed import LocalCluster
    return LocalCluster(
            processes=True,
            n_workers=2,
            threads_per_worker=8,
            memory_limit="16GB",
            )

if __name__ == "__main__":  # This avoids infinite subprocess creation
#    client = asyncio.get_event_loop().run_until_complete(get_dask_client())
    import dask
    dask.config.set({"array.slicing.split_large_chunks": False})
    dask.config.set({"array.chunk-size": "100 MB"})
    zarrcluster = asyncio.get_event_loop().run_until_complete(get_dask_cluster())
    #cluster.adapt(
    #        target_duration="0.1s",
    #        minimum=2,
    #        maximum=6,
#            minimum_cores=2,
#            maximum_cores=2,
    #        minimum_memory="16GB",
    #        maximum_memory="48GB"
    #        )
    #client=Client(cluster)
    os.environ["ZARR_ADDRESS"]=zarrcluster.scheduler._address
    cat=intake.open_catalog(CATALOG_FILE)
    hostids=[]
    for source in list(cat["model-output"]):
        if source != "csv" and source != "esm-json":
            hostids+=find_data_sources(cat["model-output"][source])
    print(hostids)
    dsdict={}
    for dsid in hostids:
        testurl=cat["model-output"][dsid].describe()["args"]["urlpath"]
        if type(testurl)==list:
            testurl=testurl[0]
        if "bm1344" not in testurl and "bk1377" not in testurl:
            continue
        if not (testurl.startswith("reference::") or testurl.startswith('"reference::') ):
            print(testurl)
            continue
        try:
            print(dsid)
            ds=cat["model-output"][dsid].to_dask()
        except Exception as e:
            print("Could not load:")
            print(e)
            continue

        if "fesom" in dsid:
            chunk_dict = dict(nod2=1000000, nz1_upper=6, nz1=6, nz_upper=6, nz=6, time=4)
            keylist = [k for k in chunk_dict.keys() if k not in ds.dims]
            for k in keylist:
                del chunk_dict[k]
            ds = ds.chunk(chunk_dict)
        ds = ds.reset_encoding()
        to_coords=[a for a in ds.data_vars if a in ["cell_sea_land_mask","lat","lon","coast"]]
        if to_coords:
            ds = ds.set_coords(to_coords)
        if dsid.startswith("ifs-amip"):
            ds = ds.rename({'value':'latlon'}).set_index(latlon=("lat","lon")).unstack("latlon")
        elif "native" in dsid:
            ds=xr.apply_ufunc(
                    compress_data,
                    ds,
                    dask="parallelized",
                    keep_attrs="drop_conflicts"
                    )
        for var in ds.data_vars:
        #                ds[var].encoding["compressor"]=None
            ds[var].encoding = {
                    'compressor': numcodecs.Blosc(cname='lz4', clevel=5, shuffle=2, blocksize=0),
                    }
        dsdict[dsid]=ds
    #collection = xp.Rest([], cache_kws=dict(available_bytes=0))
    #collection.register_plugin(DynamicKerchunk())
    collection = xp.Rest(dsdict,cache_kws=dict(available_bytes=0))    
    #collection.register_plugin(DynamicKerchunk())
    collection.register_plugin(DynamicAdd())
    collection.register_plugin(FileServe())
    collection.register_plugin(PlotPlugin())

    collection.serve(host="0.0.0.0", port=9000)
