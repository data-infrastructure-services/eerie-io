import requests
import sys
import copy
import xarray as xr
import fsspec

SERVER = sys.argv[1]
# SERVER="http://cmip-dps.cloud.dkrz.de:9000/datasets"
INTAKE_FILE = "/work/bm1344/DKRZ/intake/dkrz_eerie_xpublish.yaml"
resp = requests.get(SERVER)
dsets = resp.content.decode("utf-8")
dsets = eval(dsets)

intake_dict = dict(
    sources=dict(
        entry=dict(
            args=dict(
                consolidated=True,
                urlpath="https://eerie.cloud.dkrz.de/datasets/dset/zarr",
            ),
            driver="zarr",
            metadata=dict(
                plots=dict(
                    quadmesh=dict(
                        x="lon",
                        y="lat",
                        z="{{variable}}",
                        geo=True,
                        rasterize=True,
                        project=True,
                        #datashade=True,
                        width=800,
                        height=600
                    ),
                    line=dict(
                        x="time",
                        y="{{variable}}",
                        by=None,
                        grid=True,
                        width=800,
                        height=600
                    )
                )
            ),
            parameters=dict(
                variable=dict(
                    description="Variables in the dataset",
                    type="str",
                    default="none",
                    allowed="none"
                )
            )
        )
    )
)

for dset in dsets:
    intake_dict["sources"][dset] = copy.deepcopy(intake_dict["sources"]["entry"])
    intake_dict["sources"][dset]["args"]["urlpath"] = intake_dict["sources"][dset][
        "args"
    ]["urlpath"].replace("dset", dset)
    if not "month" in dset and not dset.endswith("atm_mon"):
        del intake_dict["sources"][dset]["metadata"]
        del intake_dict["sources"][dset]["parameters"]
    else:
        ds=xr.open_zarr(intake_dict["sources"][dset]["args"]["urlpath"])
        varlist=list(ds.data_vars)
        intake_dict["sources"][dset]["parameters"]["variable"]["default"]=varlist[0]
        intake_dict["sources"][dset]["parameters"]["variable"]["allowed"]=varlist
        ds.close()
        if dset.endswith("atm_mon"):
            del intake_dict["sources"][dset]["metadata"]["plots"]["quadmesh"]
        else:
            del intake_dict["sources"][dset]["metadata"]["plots"]["line"]
            

del intake_dict["sources"]["entry"]

import yaml

with fsspec.open(INTAKE_FILE, "w") as f:
    f.write(yaml.safe_dump(intake_dict))
