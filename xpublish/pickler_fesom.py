import sys
import pickle
from pathlib import Path
import os
import intake
import tqdm

#CATALOG = sys.argv[1]
#EXP = sys.argv[2]
CATALOG = "https://data.nextgems-h2020.eu/catalog.yaml"
EXP = "IFS_4.4-FESOM_5-cycle3"
PICKLE_TRUNK = "/work/bm1344/DKRZ/pickle"
# ngc3 fesom data:

cat = intake.open_catalog(CATALOG)
fesom_ngc3 = cat.FESOM[EXP]

for dset in tqdm.tqdm(list(fesom_ngc3)):
    key_dset = dset.replace(".", "")
    #lzarr = False
    #if "zarr" in dset:
    #    lzarr = True
    dset_source = fesom_ngc3[dset]
    lpath = False
    for path in dset_source.describe()["args"]["urlpath"]:
        lpath = False
        if "bm1344" not in path:
            print(f"{dset} bm1344 not in path")
            lpath = True
            break
    if lpath :#or not lzarr:
        continue
    print(key_dset)

    fesom_dset = dset_source.to_dask()
    fesom_dset = fesom_dset.chunk(dict(time=6))
#    chunk_dict = dict(nod2=1000000, nz1_upper=6, nz1=6, nz_upper=6, nz=6, time=4)
#    keylist = [k for k in chunk_dict.keys() if k not in fesom_dset.dims]
#    for k in keylist:
#        del chunk_dict[k]
#    fesom_dset = fesom_dset.chunk(chunk_dict)
#    for var in fesom_dset.data_vars:
#        del fesom_dset[var].encoding["chunks"]
#    for var in fesom_dset.coords:
#        if "chunks" in fesom_dset[var].encoding:
#            del fesom_dset[var].encoding["chunks"]

    EXP_NOPOINT=EXP.replace('.','').replace('_','')
    os.makedirs(f"{PICKLE_TRUNK}/{EXP_NOPOINT}", exist_ok=True)
    pkl = pickle.dumps(fesom_dset, protocol=-1)
    pp = Path(f"{PICKLE_TRUNK}/{EXP_NOPOINT}/{key_dset}.pkl")
    pp.open("wb").write(pkl)