import sys
from pathlib import Path
import os
import intake
import tqdm
import glob
import kerchunk.hdf
from kerchunk.combine import merge_vars
from kerchunk.combine import MultiZarrToZarr
import xarray as xr
import copy
import json
from multiprocessing import Pool
import zarr
import numpy as np
from copy import deepcopy
import dask
import pickle
dask.config.set({"array.chunk-size": "100 MB"})

#CATALOG = sys.argv[1]
#EXP = sys.argv[2]
CATALOG = "/work/bm1344/DKRZ/intake/dkrz_eerie-raw_disk.yaml"
#CATALOG = "https://data.nextgems-h2020.eu/catalog.yaml"
KERCHUNK_TRUNK = "/work/bm1344/DKRZ/kerchunks"
PICKLE_TRUNK = "/work/bm1344/DKRZ/pickle"
l_pickle=False
#INITINIT="1958"
INITINIT=None
# ngc3 fesom data:
#CATENTRY="FESOM.IFS_4.4-FESOM_5-cycle3"
#CATENTRY="fesom2_eerie-1950control"
#CATENTRYCLEAN="IFS4-4-FESOM-5-cycle3_FESOM"
MODEL="IFS-FESOM2-SR"
CATENTRYCLEAN="IFS-FESOM2-"
CATENTRY=None
l_setcleanentry=True

cat = intake.open_catalog(CATALOG)
  
def open_and_write_kerchunk(idx, file, kerchunks_path, exp,v):
    kerchunk_path_single = os.path.join(
        kerchunks_path, f"{exp}.{v}.json.{idx:05d}"
    )
    if not os.path.isfile(kerchunk_path_single):
        h5chunks = kerchunk.hdf.SingleHdf5ToZarr(file)
        with open(kerchunk_path_single, "wb") as f:
            f.write(json.dumps(h5chunks.translate()).encode())
        return h5chunks.translate()
    else:
        with open(kerchunk_path_single, "rb") as f:
            retval = json.load(f)            
        return retval

def open_kerchunk(out):
    return xr.open_dataset(
        "reference://",
        engine="zarr",
        backend_kwargs={
            "storage_options": {
                "fo": out
            },
            "consolidated": False,
        },
        chunks="auto"
    )
    
for exp in list(cat):
    print(exp)
    #if "zarr" in exp:
    #    continue
    dset_source = cat[exp]
    description=dset_source.describe()
    checkpath=description["args"]["urlpath"]
    if type(checkpath) == list:
        checkpath=description["args"]["urlpath"][0]
    if "bm1344" in checkpath:
        allfiles=glob.glob(checkpath)

    #assume that all paths contain bm1344
        globpath_no_vars=deepcopy(checkpath)
        if "user_parameters" in description and description["user_parameters"] != []:
            variables=description["user_parameters"][0]["allowed"]
        else:
            if "_ifs_" in exp:
                variables=list(set([a.split('/')[-1].split('_')[0] for a in allfiles]))
            else:
                if "gr" in exp:
                    variables=list(set([a.split('/')[-1].split('_')[0] for a in allfiles]))
                else:
                    variables=list(set([a.split('/')[-1].split('_')[1] for a in allfiles]))

            #variables=[
            #    a.split('/')[-1]
            #    for a in glob.glob('/'.join(checkpath.split('/')[0:-1]))
            #]
            #globpath_no_vars='/'.join(checkpath.split('/')[0:-2])
        exp_no_point=exp.replace('.','')
        kerchunks_path = os.path.join(f"{KERCHUNK_TRUNK}/{exp}")
        l_multi=True
        allkerchunkvars=[]
        kerchunk_path = os.path.join(f"{kerchunks_path}/{exp_no_point}.json")
        for v in tqdm.tqdm(variables):
            globpath = deepcopy(globpath_no_vars)
            init = INITINIT
            #
            print(v)
            os.makedirs(kerchunks_path, exist_ok=True)
            kerchunk_path_var = os.path.join(f"{kerchunks_path}/{exp_no_point}_{v}.json.var")

            if globpath != checkpath:
                globpath+=f"/{v}/*"
            print(f"Search in {globpath}")
#            files=sorted(glob.glob(globpath.replace("{{var}}",v)))
            files=[a for a in allfiles if f"{v}_" in a]
            if "fesom_ng5.nc" in files[0]:
                continue
            if init:
                while init in files[0]:
                    files = files[1:]
            else:
                try:
                    #init=int(files[0].split('_')[-1].split('-')[0])
                    print(files[0])
                    init=int(files[0].split('/')[-1].split('_')[2].split('-')[0][0:4])
                except:
                    print(f"Could not find init of {v}")
                    continue
            print(f"First year: {init}")

            #assuming yearly output frequency
            #midth_of_year=[
            #    np.datetime64(f'{str(a)}-01-01')+
            #    (np.datetime64(f'{str(a)}-12-31')- np.datetime64(f'{str(a)}-01-01')) / 2
            #    for a in range(
            #        int(init),int(init)+len(files)
            #    )
            #]
            #files=glob.glob(description["args"]["urlpath"].replace("{{var}}","*"))
            kerchunks=[]
            with Pool(min([16,len(files)])) as pool:
                kerchunks+=pool.starmap(open_and_write_kerchunk,
                                        [(idx, file, kerchunks_path, exp, v)
                                         for idx, file in enumerate(files)])
            out = kerchunks[0]
            print(len(kerchunks))
            if len(files) > 1:
                try:
                    mzz = MultiZarrToZarr(
                        kerchunks,
                        concat_dims=["time"],
                        identical_dims=["nz","nz1","nod2"],
                        #coo_map = {'year':midth_of_year},
                        #coo_dtypes={'year': np.dtype('datetime64[ns]')},
                        #concat_dims=['year'],
                    )
                    out = mzz.translate()
                    ds = open_kerchunk(out)
                    with open(kerchunk_path_var, "wb") as f:
                        f.write(json.dumps(out).encode())
                    allkerchunkvars.append(copy.deepcopy(out))
                except:
                    l_multi=False
                    pass

        if not l_multi and l_pickle :      
            pickles_path = os.path.join(f"{PICKLE_TRUNK}/{exp}")
            fesom_dset=dset_source.to_dask()
            os.makedirs(f"{pickles_path}", exist_ok=True)
            pkl = pickle.dumps(fesom_dset, protocol=-1)
            pp = Path(f"{pickles_path}/{exp_no_point}.pkl")
            pp.open("wb").write(pkl)
        else:
            out=merge_vars(allkerchunkvars)            
            with open(kerchunk_path, "wb") as f:
                f.write(json.dumps(out).encode())

