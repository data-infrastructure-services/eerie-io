import sys
from pathlib import Path
import os
from tqdm import tqdm
import glob
import kerchunk.hdf
from kerchunk.combine import merge_vars
from kerchunk.combine import MultiZarrToZarr
from kerchunk import df
import math
import xarray as xr
import copy
import json
from multiprocessing import Pool
from copy import deepcopy
import zarr
import re
from datetime import datetime, timedelta
import numpy as np
import shutil

#CATALOG = "https://data.nextgems-h2020.eu/catalog.yaml"
KERCHUNK_TRUNK = "/work/bm1344/DKRZ/kerchunks_batched"
PICKLE_TRUNK = "/work/bm1344/DKRZ/pickle"

EXP="eerie-control-1950"
HOURS_SINCE='1950-01-01'
MODEL="IFS-FESOM2-SR"
EXPTRUNK="/work/bm1344/a270228/EERIE_Hackathon/IFS-FESOM_CONTROL-1950/tco1279-NG5/"
EXPTRUNK="/work/bm1344/a270228/EERIE_NextG_Hackathon/IFS-FESOM_CONTROL-1950/tco1279-NG5/"
#CATALOG = "https://data.nextgems-h2020.eu/catalog.yaml"
KERCHUNK_TRUNK = "/work/bm1344/DKRZ/kerchunks_batched"
kerchunks_path='/'.join([KERCHUNK_TRUNK,'_'.join([MODEL,EXP])])
kerchunks_path_single=kerchunks_path.replace('batched','single')
patterns=sorted(os.listdir(kerchunks_path_single))

FILES_PRO_BATCH=50

l_pickle=False
#INITINIT="1958"
  
def open_and_load_kerchunk_path(kerchunk_path_single):
    with open(kerchunk_path_single, "rb") as f:
        return json.load(f)            

def open_kerchunk(out):
    return xr.open_dataset(
        "reference://",
        engine="zarr",
        backend_kwargs={
            "storage_options": {
                "fo": out
            },
            "consolidated": False,
        },
        chunks="auto"
    )

def create_local_grid_and_gridchunk(gridpath,kerchunks_path):
    gridname=gridpath.split('/')[-1]
    gf=kerchunks_path+"/"+gridname
    kf=gf.replace('.nc','.json')
    kf=kf.replace('.grib','.json')
    
    #if not os.path.isfile(kf):
    if True:
        print(f"Create grid description {kf}")

        if gridname.endswith(".grib"):
            print("starting grib encription")
            gridds=xr.open_dataset(gridpath, engine="cfgrib")
            coord_names=["latitude","longitude"]

            coords= gridds[coord_names].rename(
                {
                    "values":"rgrid",
                }
            )
            coords.attrs={}
            coords.to_netcdf(gf, mode="w")
            gridkc = kerchunk.hdf.SingleHdf5ToZarr(gf)
            with open(kf, "wb") as f:
                f.write(json.dumps(gridkc.translate()).encode())

            gridds.close()
        else:
            gridds=xr.open_dataset(gridpath)
            coord_names=["grid_center_lon","grid_center_lat","coast"]

            coords= gridds[coord_names].rename(
                {
                    "grid_size":"x",
                    "grid_center_lon":"lon",
                    "grid_center_lat":"lat"
                }
            )
            coords.attrs={}
            coords.to_netcdf(gf, mode="w")
            gridkc = kerchunk.hdf.SingleHdf5ToZarr(gf)
            with open(kf, "wb") as f:
                f.write(json.dumps(gridkc.translate()).encode())

            gridds.close()
    return gridname

def pre_process(refs):
    reflist = list(refs)
    if any(k.startswith("reduced_points") for k in reflist):
        for k in reflist:
            if k.startswith('reduced_points'):
                refs.pop(k)
            elif k.startswith('lat/'):
                refs.pop(k)
    return refs

def modify_coords_and_atts(out):
    global pattern
    gl="native"
    if "025_" in  pattern:
        gl="gr025"
    updatedict=dict(
        DOKU_Name=f"EERIE {MODEL} {EXP} run",
        DOKU_summary=f"EERIE {MODEL} {EXP} run",
        DOKU_responsible_person="Fabian Wachsmann",
        DOKU_authors="Ghosh, R.; Koldunov, N.; Wachsmann, F.",
        DOKU_License="CC BY 4.0",
        grid_label=gl,
        member_id="r1i1p1f1",
        activity_id="EERIE",
        source_type="AOGCM",
        experiment_id=EXP,
        source_id=MODEL,
        history="deleted for convenience"
    )
    out_ = zarr.open(out)
    vs=list(out_.array_keys())

    if "time" in vs:
        updatedict.update(
            dict(
                time_min=out_.time[0],
                time_max=out_.time[-1]
            )
        )
    if gl == "native":
        if "coordinates" in out_.attrs:
            if "lon" in vs:
                out_.attrs["coordinates"]+=" lon lat"
            else:
                out_.attrs["coordinates"]+=" longitude latitude"

        else:
            if "lon" in vs:
                updatedict.update(dict(coordinates="lon lat"))
            else:
                updatedict.update(dict(coordinates="longitude latitude"))

    out_.attrs.update(updatedict)

    return out

def postprocess(out):
    out = modify_coords_and_atts(out)
    return out

def preprocess(refs):
    ds=open_kerchunk(refs)
    ds=xr.open_zarr(refs)
    refs["time_mod"]=ds["time"].values
    return refs
    
def kerchunk_by_pattern(pattern, allfiles, var,pp):    
    files = [a for a in allfiles if f"/{var}_" in a]
    if not files:
        files = [a for a in allfiles if f"/fesom_{var}_" in a]
    if not files:
        raise ValueError(f"Could not find files for var {var} in pattern {pattern}")
    concat_dims=["time","plev"]
    if "FESOM" in pattern or "2d" in pattern or "original" in pattern:
        concat_dims=["time"]
    return MultiZarrToZarr(
        [open_and_load_kerchunk_path(a) for a in files],
        concat_dims=concat_dims,
        identical_dims=["lat","lon","nz","nz1","nod2"],
        postprocess=postprocess,
        coo_map={"time":"cf:time"},
        preprocess=pre_process
        #    out=outp
    ).translate()
    
global pattern

for pattern in tqdm(patterns):
    if not "original" in pattern:
        continue
    combined_parq= os.path.join(kerchunks_path, pattern, f"combined.parq")
    if os.path.exists(combined_parq):
        #continue
        shutil.rmtree(combined_parq)            
    os.makedirs(combined_parq)
    #outp = LazyReferenceMapper.create(
    #    1000,combined_parq,fs
    #)
    print(
        "----------------------\n"
        f"for pattern {pattern} in {kerchunks_path_single}\n"
    )
    pp=False
    files=sorted(
        glob.glob(
            os.path.join(
                kerchunks_path_single, pattern, '*.json'
            ),
            recursive=True
        )
    )
    if not files:
        print("no files were found")
    else:
        print(
            f"{len(files)} files were found\n"
        )
        fns=[a.split('/')[-1] for a in files]
        if fns[0].startswith("fesom_"):
            fns=[a.replace("fesom_","") for a in fns]
        uniquevars=list(set([a.split('_')[0] for a in fns]))
        print(f"with {len(uniquevars)} no of unique vars:")
        print(uniquevars)
        jsons_per_var=[]
        gridname=None
        if "native" in files[0]:
            if "native_monthly_nod" in files[0] or "daily" in files[0]:
                gridname=create_local_grid_and_gridchunk('/work/bm1235/a270046/meshes/NG5_griddes_nodes_IFS.nc', kerchunks_path)
            if "native_monthly_elem" in files[0]:
                gridname=create_local_grid_and_gridchunk('/work/bm1235/a270046/meshes/NG5_griddes_elems_IFS.nc', kerchunks_path)
        elif "original" in files[0]:
            gridname=create_local_grid_and_gridchunk('/work/bm1344/a270228/EERIE_NextG_Hackathon/IFS-FESOM_CONTROL-1950/tco1279-NG5/10u_10_19610101-19610131_daily_origin_grid.grib', kerchunks_path)
        with Pool(min(20,len(uniquevars))) as pool:
            jsons_per_var+=pool.starmap(kerchunk_by_pattern,[(pattern, files, var, pp) for var in uniquevars])
            
        combined_jsons=merge_vars(jsons_per_var)
        if gridname:
            gridnameload=gridname.replace('.nc','.json').replace('.grib','.json')
            print(kerchunks_path+"/"+gridnameload)
            gridout=open_and_load_kerchunk_path(
                kerchunks_path+"/"+gridnameload
            )
            combined_jsons=merge_vars(
                [gridout,combined_jsons]
            )

        df.refs_to_dataframe(combined_jsons, combined_parq)

