import sys
from pathlib import Path
import os
from tqdm import tqdm
import glob
import kerchunk.hdf
import xarray as xr
import copy
import json
from multiprocessing import Pool
import numpy as np
from copy import deepcopy

#CATALOG = sys.argv[1]
#EXP = sys.argv[2]
EXP="eerie-control-1950"
MODEL="IFS-FESOM2-SR"
EXPTRUNK="/work/bm1344/a270228/EERIE_Hackathon/IFS-FESOM_CONTROL-1950/tco1279-NG5/"
#CATALOG = "https://data.nextgems-h2020.eu/catalog.yaml"
KERCHUNK_TRUNK = "/work/bm1344/DKRZ/kerchunks_single"
kerchunks_path='/'.join([KERCHUNK_TRUNK,'_'.join([MODEL,EXP])])
PICKLE_TRUNK = "/work/bm1344/DKRZ/pickle"
l_pickle=False
#INITINIT="1958"
INITINIT=None

pathdict={}
patterns=[
   "IFS/025/daily/3d/*.nc",
   "IFS/025/daily/2d/*.nc",
    "FESOM/025/daily/*.nc",
    "FESOM/025/monthly/*.nc",
    "FESOM/native/daily/*.nc",
    "FESOM/native/native1/monthly/*.nc",
    "FESOM/native/native2/monthly/*.nc"
]

for y in sorted(os.listdir(EXPTRUNK)):
    pathdict[y]=dict()
    for pattern in patterns:
        pathdict[y][pattern]=sorted(
            glob.glob(
                '/'.join(
                    [EXPTRUNK,y,pattern]
                )
            )
        )

def open_and_write_kerchunk(file,kf):
    h5chunks = kerchunk.hdf.SingleHdf5ToZarr(file)
    jsonh5 = h5chunks.translate()
    with open(kf, "wb") as f:
        f.write(json.dumps(jsonh5).encode())

def map_to_kerchunk(file, kerchunk_path):
    kerchunk_file='/'.join([kerchunk_path,file.split('/')[-1].replace('.nc','.json')])
    if not os.path.isfile(kerchunk_file):
        return kerchunk_file
    return None
    
for y in tqdm(pathdict.keys(),total=len(pathdict.keys())):
    for p in patterns:
        files=pathdict[y][p] 
        pattern_fine=p.replace('/','_').replace('_*.nc','').replace('native1','nod').replace('native2','elem')
        kerchunk_path='/'.join([
            kerchunks_path,
            pattern_fine
        ])
        print(kerchunk_path)
        os.makedirs(kerchunk_path,exist_ok=True)
        kerchunk_files=[]
        with Pool(64) as pool:
            kerchunk_files = pool.starmap(map_to_kerchunk,[(file, kerchunk_path) for file in files])

        if not kerchunk_files:
            print(f"Could not find any kerchunk_files for year {y} and pattern {p}")
            continue
        newfiles={
            f : kerchunk_files[i]
            for i,f in enumerate(files)
            if kerchunk_files[i]
        }

        kerchunks=[]
        with Pool(64) as pool:
            kerchunks+=pool.starmap(open_and_write_kerchunk,[(f,k) for f,k in newfiles.items()])